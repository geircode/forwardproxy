# Commands used in this setup

## SSH KEY Certificate

First, create certificate outside the volume share:

```shell
mkdir /ubuntu
ssh-keygen -t rsa -b 4096 -C geircode@geircode.no -f /ubuntu/geircode_231ab92a_rsa
```

Copy the certificate files to local volume share for persistance:

```shell
cp -r /ubuntu /app/ubuntu
```

When creating the new Container based on existing certificates, then copy the certficates to the outside of the shared volume and change the file mode:

```shell
cp -r /app/ubuntu /ubuntu
chmod 0400 /ubuntu/geircode_231ab92a_rsa.pub
chmod 0400 /ubuntu/geircode_231ab92a_rsa
```

ssh -i /ubuntu/geircode_231ab92a_rsa docker@<URL>