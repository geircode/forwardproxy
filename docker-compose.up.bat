cd %~dp0
docker rm -f forwardproxy_231ab92a-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build --remove-orphans
REM wait for 1-2 seconds for the container to start
pause
docker exec -it forwardproxy_231ab92a-1 /bin/bash