#!/bin/sh

cd /app
docker build -f Dockerfile -t geircode/forwardproxy_231ab92a .
docker-compose -f docker-compose.yml up -d --build --remove-orphans
docker exec -it forwardproxy_231ab92a-1 /bin/bash
