#!/bin/sh

cat /run/secrets/dockerloginpassword | docker login --username geircode --password-stdin
cd /app
docker build -f Dockerfile -t geircode/forwardproxy:latest .
docker push geircode/forwardproxy:latest